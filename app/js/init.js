function createCookie(name, value, s) {
    if (s) {
        var date = new Date();
        date.setMonth(date.getMonth() + s)
        var expires = "; expires=" + date.toGMTString();
    } else
        var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    } else {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
            end = dc.length;
        }
    }
    return unescape(dc.substring(begin + prefix.length, end));
}

$(document).ready(function () {

    $('.akt-bloks > div').matchHeight();
    $('.kontakt-blocks > div').matchHeight();

    $(".drop span").on("click", function () {
        var el = $(this);
        if (el.hasClass('active')) {
            el.removeClass('active');
            el.parent().find('ul').stop().slideUp();
        } else {
            $('.drop ul').stop().slideUp();
            $('.drop .active').removeClass('active');
            el.addClass('active');
            el.parent().find('ul').stop().slideDown();
        }
    });

    $('#cookies-info .close').on('click', function () {
        $('#cookies-info').animate({
            'height': '0',
            'min-height': 0
        }, 300, function () {
            $(this).hide()
        });
        createCookie("cookies_info", 1, 12);
    });

    var myCookie = getCookie("cookies_info");

    if (myCookie == null) {
        $('#cookies-info').show();
    }

    function lastAddedLiveFunc() {
        if ($('#lastPostsLoader').length) {
            if ($('#lastPostsLoader').html() == '') {
                $('#lastPostsLoader').html('<div class="loading"></div>');
                $.ajax({
                    type: 'POST',
                    url: "getPosts.php",
                    data: {
                        'offset': $('#lastPostsLoader').attr('data-offset')
                    },
                    dataType: 'json',
                    success: function (data) {
                        var divPostLoader = $('#lastPostsLoader');
                        divPostLoader.before(data.list);
                        divPostLoader.remove();
                        $('.akt-bloks > div').matchHeight();
                        $.fn.matchHeight._update();
                    },
                    error: function () {
                        $('#lastPostsLoader').empty();
                    }
                });
            }
        }
    };

    $('.load-more').on("click", function (e) {
        e.preventDefault();
        lastAddedLiveFunc();
    });

    $(window).load(function () {
        $('.owl-carousel').owlCarousel({
            margin: 0,
            items: 1,
            nav: true,
            loop: true,
            lazyLoad: true,
            autoplay: false,
            autoplayTimeout: 10000,
            autoHeight: false,
            onInitialized: function () {
                $('.slider .loading').hide();
            }
        });
    });


});