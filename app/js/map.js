function initMap() {

    var contentString = '<b style="font-size:14px;">Res-Med</b><br><p class="mb0">Ludowa 7, 05-816 Michałowice-Osiedle</p><p class="mb0">tel: <a href="tel:22 753-04-04">22 753-04-04</a></p><p class="mb0">e-mail: <a href="mailto:rejestracja@resmed40.pl">rejestracja@resmed40.pl</a></p>';

    var myLatLng = {
        lat: 52.171078,
        lng: 20.888627
    };

    var center = {
        lat: 52.185078,
        lng: 20.888627
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
        center: center,
        scrollwheel: false
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: 'images/marker.png'
    });

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
    google.maps.event.addDomListener(window, "resize", function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });
}