var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
var runSequence = require('run-sequence');
var del = require('del');
var cache = require('gulp-cache');
var critical = require('critical');
var uncss = require('gulp-uncss');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var csscomb = require('gulp-csscomb');
var tinypng = require('gulp-tinypng');
var plumber = require('gulp-plumber');

gulp.task('tinypng', function () {
    gulp.src(['app/images/*.png','app/images/*.jpg'])
        .pipe(tinypng('cj6xLMCMng-Lch7AXNm-0K3o3wF31RTT'))
        .pipe(gulp.dest('compressed_images'));
});


gulp.task('styles', function() {
  return gulp.src(['app/css/*.css', '!app/css/bootstrap.css' ,'!app/css/bootstrap-theme.css'])
    .pipe(csscomb())
    .pipe(gulp.dest('temp/css'));
});

gulp.task('concat-css', function() {
  return gulp.src(['temp/css/*.css'])
    .pipe(concat('all.css'))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('compress', function () {
	return gulp.src('app/images/*')
		.pipe(cache(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}]
		})))
		.pipe(gulp.dest('dist/images'));
});


gulp.task('sass', function () {
  return gulp.src('app/scss/**/*.scss')
    .pipe(plumber())
    .pipe(sass({
          includePaths: require('node-normalize-scss').includePaths
      }))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('useref', function(){
  return gulp.src('app/*.html')
        .pipe(useref())
//        .pipe(gulpif('*.js', uglify()))
        .pipe(gulp.dest('dist'));
});

gulp.task('fonts', function() {
  return gulp.src('app/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'))
});

gulp.task('favicons', function() {
  return gulp.src('app/favicons/**/*')
  .pipe(gulp.dest('dist/favicons'))
})

gulp.task('other', function() {
  gulp.src(['app/browserconfig.xml', 'app/manifest.json', 'app/favicon.ico', 'app/getPosts.php'])
      .pipe(gulp.dest('dist'))
  gulp.src(['app/js/jquery-1.11.0.min.js'])
      .pipe(gulp.dest('dist/js'));
});

gulp.task('del', function() {
    gulp.src('dist/css/all.min.css')
    del.sync(['dist/css/all.min.css']);   
});

gulp.task('rename', function() {
     return gulp.src('dist/css/min.css')
    .pipe(rename('all.min.css'))
    .pipe(gulp.dest('dist/css'));
});


gulp.task('minify-css', function() {
    return gulp.src('dist/css/all.css')
        .pipe(cleanCSS({debug: true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(rename('min.css'))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('ucss', function() {
    return gulp.src(['app/css/bootstrap.css','app/css/bootstrap-theme.css'])
    .pipe(uncss({
      html: [
        './app/index.html',
        './app/news.html',
        './app/kontakt.html',
        './app/text.html',
        './app/harmonogram.html'
      ]
    }))
        .pipe(gulp.dest('temp/css'));
});




gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'app'
    },
  })
})

gulp.task('watch', ['browserSync', 'sass'], function (){
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/*.html', browserSync.reload); 
  gulp.watch('app/js/**/*.js', browserSync.reload); 
});

gulp.task('clean', function() {
  return del.sync('dist').then(function(cb) {
    return cache.clearAll(cb);
  });
})

gulp.task('clean:dist', function() {
  return del.sync(['dist/**/*', '!dist/images', '!dist/images/**/*']);
});

gulp.task('build', function (callback) {
  runSequence('clean:dist', ['sass', 'useref', 'compress', 'fonts','favicons','other' ], 'styles', 'ucss', 'concat-css', callback)
})

gulp.task('default', function (callback) {
  runSequence(['sass','browserSync', 'watch'], callback)
})